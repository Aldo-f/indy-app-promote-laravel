<?php

namespace App;

use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;

class Page extends \TCG\Voyager\Models\Page
{
    use Resizable;

}
