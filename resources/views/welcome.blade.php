@include('partials.header')

	<!-- end header Area -->

	<!-- start banner Area -->
	<section class="home-banner-area">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="home-banner-content col-lg-6 col-md-6">
					<h1>
						App That <br> Suits You Better
					</h1>
					<p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards.</p>
					<div class="download-button d-flex flex-row justify-content-start">
						<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on App Store
									</p>
								</a>
							</div>
						</div>
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="banner-img col-lg-4 col-md-6">
					<img class="img-fluid" src="/storage/img/banner-img.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start fact Area -->
	<section class="fact-area">
		<div class="container">
			<div class="fact-box">
				<div class="row align-items-center">
					<div class="col single-fact">
						<h2>100K+</h2>
						<p>Total Downloads</p>
					</div>
					<div class="col single-fact">
						<h2>10K+</h2>
						<p>Positive Reviews</p>
					</div>
					<div class="col single-fact">
						<h2>50K+</h2>
						<p>Daily Visitors</p>
					</div>
					<div class="col single-fact">
						<h2>0.02%</h2>
						<p>Uninstallation Rate</p>
					</div>
					<div class="col single-fact">
						<h2>15K+</h2>
						<p>Pro User</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End fact Area -->

	<!-- Start feature Area -->
	<section class="feature-area section-gap-top">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Unique Features</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Expert Technicians</h3>
						</a>
						<p>
							Usage of the Internet is becoming more common due to rapid advancement of technology and power.
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Professional Service</h3>
						</a>
						<p>
							Usage of the Internet is becoming more common due to rapid advancement of technology and power.
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Great Support</h3>
						</a>
						<p>
							Usage of the Internet is becoming more common due to rapid advancement of technology and power.
						</p>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End feature Area -->

	<!-- Start about Area -->
	<section class="about-area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 home-about-left">
					<img class="img-fluid" src="/storage/img/iphone.png" alt="">
				</div>
				<div class="offset-lg-1 col-lg-5 home-about-right">
					<h1>
						We Believe that <br>
						Interior beautifies the <br>
						Total Architecture
					</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
						magna aliqua.Ut
						enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
					</p>
					<a class="primary-btn text-uppercase" href="#">Get Details</a>
				</div>
				<div class="col-lg-6 home-about-right home-about-right2">
					<h1>
						We Believe that <br>
						Interior beautifies the <br>
						Total Architecture
					</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
						magna aliqua.Ut
						enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
					</p>
					<div class="download-button d-flex flex-row justify-content-start">
						<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on App Store
									</p>
								</a>
							</div>
						</div>
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 home-about-left">
					<img class="img-fluid" src="/storage/img/iphone.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End about Area -->

	<!-- Start about-video Area -->
	<section class="about-video-area section-gap">
		<div class="vdo-bg">
			<div class="container">
				<div class="row align-items-center justify-content-center">
					<div class="col-lg-12 about-video-right justify-content-center align-items-center d-flex relative">
						<div class="overlay overlay-bg"></div>
						<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid mx-auto" src="/storage/img/play-btn.png"
							 alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End about-video Area -->



	<!-- Start Screenshot Area -->
	<section class="screenshot-area section-gap-top">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Featured Screens</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="owl-carousel owl-screenshot">
					<div class="single-screenshot">
						<img class="img-fluid" src="/storage/img/screenshots/s1.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="/storage/img/screenshots/s2.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="/storage/img/screenshots/s3.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="/storage/img/screenshots/s4.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Screenshot Area -->

	
    @include('partials.footer')
